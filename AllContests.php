<?php
try {
	require 'Main/Authentication.php';
	$auth = new Main\Authentication;
	$header = Array("Accept: application/json", "Authorization: Bearer " . $_SESSION['accessToken']);
	$contestUrl = $auth->apiEndpoint . 'contests?';
	$arrayPrm = [
		'limit' => '100',
		'offset' => '0',
		'status' => 'present',
	];
	$contestUrl = $contestUrl . http_build_query($arrayPrm);
	$output = $auth->make_curl_request($contestUrl, false, $header);
	$output = json_decode($output);
/*var_dump($output);*/
	$contests[] = (array) $output->result->data->content->contestList;
	$arrayPrm = [
		'limit' => '100',
		'offset' => '0',
		'status' => 'past',
	];
	$contestUrl = $contestUrl . http_build_query($arrayPrm);
	$output = $auth->make_curl_request($contestUrl, false, $header);
	$outputs = json_decode($output);
	foreach ($outputs->result->data->content->contestList as $output) {
		$contests[0][] = $output;
	}

	$contests = $contests[0];

	$problemUrl = $auth->apiEndpoint . 'users/me';
	$problemUrl = $problemUrl;

	$output = $auth->make_curl_request($problemUrl, false, $header);
	$output = json_decode($output);

	$myData = (array) $output->result->data->content;

/*var_dump($myData);*/
} catch (Exception $e) {
	header("Location: " . $auth->websiteBaseUrl . "/Error.php");
	die();
}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>All Contests</title>
		<?php require "ExternalLinks.php"?>
	</head>

	<body>
		<style type="text/css">
			#heading{
				background-color: #2b3b90;
				color: white;
				padding: 1%;
				display: flex;
				justify-content: space-between;
				font-size: 14px;
			}
			#contestTable{
				margin: 1% 5%;
			    padding: 2%;
			    font-size: 12px;
			    font-weight: 400 !important;
			}
			.pbName{
				font-weight: 400;
			}
			.pbSub{
				font-weight: 400;
				text-align: center !important;
			}
			.pbAc{
				font-weight: 400;
				text-align: center !important;
			}
			.headingTxt{
				text-align: center !important;
				vertical-align: center;
			}
			#contestTable{
				font-size: 12px !important;
			}
		</style>
		<div id="heading">
			<div>
				Codechef contests
			</div>
			<div>
				<div>
				<i class="fa fa-user" style="margin-right: 5px;"></i><?php echo $myData['username'] ?> ( <?php echo $myData['band'] ?> )
				</div>
			</div>
		</div>
		<div id="contestTable" class="card">
			<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth" id="contestTableData">
				<thead>
					<tr style="background-color: #25b945;">
						<th class="headingTxt" style="color:white;">Name</th>
						<th class="headingTxt" style="color:white;">Start Date</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($contests as $contest) {?>
					<tr >
						<th class="pbName" ><a href="./ShowLeaderBoard.php?ccode=<?php echo $contest->code ?>"><?php echo $contest->name ?></a></th>
						<th class="pbSub"><?php echo $contest->startDate ?></th>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
		<script type="text/javascript">
			$('#contestTableData').DataTable({
	            "order": [[ 1, "desc" ]],
	            "pageLength": 100,
	            "autoWidth": false,
			});
		</script>
	</body>
</html>