<?php require 'Main/Authentication.php';
try {

	$auth = new Main\Authentication;
	$header = Array("Accept: application/json", "Authorization: Bearer " . $_SESSION['accessToken']);
	$problemUrl = $auth->apiEndpoint . 'users/me';
	$problemUrl = $problemUrl;

	$output = $auth->make_curl_request($problemUrl, false, $header);
	$output = json_decode($output);

	$myData = (array) $output->result->data->content;
	$country = $myData['country']->name;
/*var_dump($myData);*/
} catch (Exception $e) {
	header("Location: " . $auth->websiteBaseUrl . "/Error.php");
	die();
}

?>
<!DOCTYPE html>
<html>
<head>
	<?php require "ExternalLinks.php"?>
	<title>Leader Board</title>
</head>
<body>
<style type="text/css">
body{
	font-size: 11px !important;
}
  .lds-spinner {
  color: official;
  display: inline-block;
  position: relative;
  width: 80px;
  height: 80px;
  left:2%;
}
.lds-spinner div {
  transform-origin: 5px 40px;
  animation: lds-spinner 1.2s linear infinite;
}
.lds-spinner div:after {
  content: " ";
  display: block;
  position: absolute;
  /*top: 3px;
  left: 37px;*/
  width: 6px;
  height: 18px;
  border-radius: 20%;
  background: #2b3b90;
  margin:auto;
}
.lds-spinner div:nth-child(1) {
  transform: rotate(0deg);
  animation-delay: -1.1s;
}
.lds-spinner div:nth-child(2) {
  transform: rotate(30deg);
  animation-delay: -1s;
}
.lds-spinner div:nth-child(3) {
  transform: rotate(60deg);
  animation-delay: -0.9s;
}
.lds-spinner div:nth-child(4) {
  transform: rotate(90deg);
  animation-delay: -0.8s;
}
.lds-spinner div:nth-child(5) {
  transform: rotate(120deg);
  animation-delay: -0.7s;
}
.lds-spinner div:nth-child(6) {
  transform: rotate(150deg);
  animation-delay: -0.6s;
}
.lds-spinner div:nth-child(7) {
  transform: rotate(180deg);
  animation-delay: -0.5s;
}
.lds-spinner div:nth-child(8) {
  transform: rotate(210deg);
  animation-delay: -0.4s;
}
.lds-spinner div:nth-child(9) {
  transform: rotate(240deg);
  animation-delay: -0.3s;
}
.lds-spinner div:nth-child(10) {
  transform: rotate(270deg);
  animation-delay: -0.2s;
}
.lds-spinner div:nth-child(11) {
  transform: rotate(300deg);
  animation-delay: -0.1s;
}
.lds-spinner div:nth-child(12) {
  transform: rotate(330deg);
  animation-delay: 0s;
}
@keyframes lds-spinner {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
}
.hidden{
	display:none !important;
}
#header{
	background-color :#2b3b90;
	font-size: 14px !important;
	color:white;
	padding:1%;
	margin-bottom: 1%;
	display:flex;
}
td{
	padding:1% !important;
	text-align: center !important;
}
#contest:hover{
	text-decoration: underline;
}
</style>
<div id="content">
<div id="header">
	<div style="flex-basis: 50%;">
	Top 3 Institutions participated <?php echo "in " . $myData['country']->name ?>
	</div>
	<div style="display: flex;justify-content: flex-end;flex-basis: 50%;">
		<div style="margin:0% 3%;" id="contest">
			<a href="/AllContests.php" style="color:white;">All Contests</a>
		</div>
		<div>
			<i class="fa fa-user" style="margin-right: 5px;"></i><?php echo $myData['username'] ?> ( <?php echo $myData['band'] ?> )
		</div>
	</div>
</div>
<div id="spinner" style="height: 550px;display: flex;justify-content: center;align-items: center;flex-direction: column;">
	<div>
		<div id="timer" style="font-size: 16px !important; text-align: center;"></div>
	</div>
	<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
</div>
<div id="leaderContent">
	<div style="display: flex;justify-content: center;align-items: center;margin:1%;">
		<div style="background-color: #DAA520;height:200px;width:400px;cursor:pointer;display: flex;justify-content: center;align-items: center;" v-on:click="showStudents(topThree[0])" class="animated fadeInDown card">
			<div style="flex-basis: 28%;">
				<img src="/images/gold.png" style="height: auto;width:142px;">
			</div>
			<div style="text-align: center;color:white;font-size: 15px !important;flex-basis: 72%;padding:1%;">
				{{topThree[0]}}<br>
				Score : {{Score[0]}}
			</div>
		</div>
	</div>
	<div style="display: flex;justify-content: space-around;align-items: center;margin: 1%;">
		<div style="background-color: #888888;height:200px;width:400px;cursor: pointer;display: flex;justify-content: center;align-items: center;" v-on:click="showStudents(topThree[1])" class="animated fadeInLeft card">
			<div style="flex-basis: 28%;">
				<img src="/images/silver.png" style="height: auto;width:142px;">
			</div>
			<div style="text-align: center;color:white;font-size: 15px !important;flex-basis: 72%;padding: 1%;">
				{{topThree[1]}}<br>
				Score : {{Score[1]}}
			</div>
		</div>
		<div style="background-color: #cd7f32;height:200px;width:400px;cursor: pointer;display: flex;justify-content: center;align-items: center;" v-on:click="showStudents(topThree[2])" class="animated fadeInRight card">
			<div style="flex-basis: 28%;">
				<img src="/images/bronze.png" style="height: auto;width:142px;">
			</div>
			<div style="text-align: center;color:white;font-size: 15px !important;flex-basis: 72%;padding:1%;">
				{{topThree[2]}}<br>
				Score : {{Score[2]}}
			</div>
		</div>
	</div>
</div>
<div id="studentContent" class="card animated zoomIn hidden" style="margin: 1% 24%;padding: 1%;">
	<div style="margin: 1%;">
	<a class="button is-small" v-on:click="showLeaderBoard()" style="border-radius: 0px;"><i class="fa fa-arrow-left"></i> <div style="margin-left: 10%;">Go Back</div></a>
	</div>
	<div>
		<table id="studentTable" class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
			<thead style="font-size: 14px;">
				<tr style="background-color: #25b945;">
					<td style="color:white;">Rating</td>
					<td style="color:white;">User Name</td>
					<td style="color:white;">Total Score</td>
				</tr>
			</thead>
		</table>
	</div>
</div>
<div id="modals">
<div class="modal" v-bind:class="{'is-active':WaitModalActive}">
  <div class="modal-background"></div>
  <div class="modal-card animated zoomIn">
    <header class="modal-card-head">
      <p class="modal-card-title">Waiting...</p>
    </header>
    <section class="modal-card-body">
      <p style="font-size: 15px;">Please Wait for 5 Mins to fetch the full data.</p>
      <p style="font-size: 15px;">The api doesn't allow multiple requests within 5 mins</p>
      <div id="clockdiv" style="font-size: 15px;"></div>
    </section>
    <footer class="modal-card-foot" style="justify-content: flex-end;">
      <button class="button is-success" v-on:click="reload">Reload</button>
    </footer>
  </div>
</div>
<div class="modal" v-bind:class="{'is-active':NoDataModalActive}">
  <div class="modal-background"></div>
  <div class="modal-card animated zoomIn">
    <header class="modal-card-head">
      <p class="modal-card-title">No Data !</p>
    </header>
    <section class="modal-card-body">
      <p style="font-size: 13px;">No Data Found</p>
    </section>
    <footer class="modal-card-foot" style="justify-content: flex-end;">
      <a class="button is-success" href="/AllContests.php"><< Go Back</a>
    </footer>
  </div>
</div>

</div>
</div>
<script type="text/javascript">

	class Institution {
	  constructor(name) {
	  	this.name=name;
	    this.count = 0;
	    this.totalScore = 0;
	    this.average=0;
	  }
	}
	var content=new Vue({
		el:'#content',
		data:{
			i:0,
			url:"",
			showAlert:true,
			accessToken:"<?php echo $_SESSION['accessToken'] ?>",
			contestCode:"<?php echo $_GET['ccode'] ?>",
			output:[],
			temp:[],
			institutions:[],
			avgInstitute:'',
			avgInstituteArray:null,
			topThree:[],
			showParticipatedStudents:'',
			dataTable:null,
			studentTableData:[],
			WaitModalActive:false,
			NoDataModalActive:false,
			Score:[],
		},
		created:function()
		{
			$('#leaderContent').addClass('hidden');
			this.url="<?php echo $auth->apiEndpoint ?>"+'rankings/'+this.contestCode+'?sortBy=rank&country='+"<?php echo $country ?>"+'&sortOrder=asc&limit=1500&offset=';
			for(this.i=0;this.i<3;this.i++)
			{

				axios({
				  method: 'get',
				  url: this.url+((this.i)*1500),
				  headers: {
				  	'Accept': 'application/json',
				  	'Authorization' : "Bearer "+this.accessToken,
				  }
				}).then(function(data){
					console.log(data);
					this.temp=data.data.result.data.content;
					console.log(this.temp);

					for(j=0;j<this.temp.length;j++)
					{

						  var p=[];
						  p['contestCode'] = this.temp[j].contestCode;
						  p['country']=this.temp[j].country;
						  p['countryCode']=this.temp[j].countryCode;
						  p['institution']=this.temp[j].institution;
						  p['institutionType']=this.temp[j].institutionType;
						  p['penalty']=this.temp[j].penalty;
						  p['problemScore']=this.temp[j].problemScore;
						  p['rank']=this.temp[j].rank;
						  p['rating']=this.temp[j].rating;
						  p['totalTime']=this.temp[j].totalTime;
						  p['username']=this.temp[j].username;
						  p['totalScore']=this.temp[j].totalScore;
						  try
						  {
						  		var institute;
						  		if(this.institutions[this.temp[j].institution]===undefined)
								  {

								  	 institute=new Institution(this.temp[j].institution);

								  }
								  else
								  {
								    institute=this.institutions[this.temp[j].institution];

								  }

								  institute.count++;
								  institute.totalScore= (parseFloat(institute.totalScore)+parseFloat(this.temp[j].totalScore));
								  var avg=(parseFloat(institute.totalScore)/parseFloat(institute.count));
								  institute.average=avg.toFixed(2);
								  this.institutions[this.temp[j].institution]=institute;
						  }catch(e)
						  {
						  	console.log(e);
						  }
						this.output.push(p);
					}
					//console.log(this.output);
				}.bind(this))
				.catch(e => {
					console.log(e);
					console.log(e.response.status);
					if(e.response.status==429 && this.showAlert==true)
					{
						/*alert("Wait for 5 mins to view the full data");*/
						this.showAlert=false;
						this.topThree=[];
						this.WaitModalActive=true;
					}

				});
			}
			this.avgInstitute=new SortedMap();
			setTimeout(function()
			{
				if(this.output.length===0 && this.WaitModalActive===false)
				{
					this.NoDataModalActive=true;
				}
				else
				{
					for(var index in content.institutions)
					{

						content.avgInstitute.set(content.institutions[index].average,content.institutions[index].name);
					}
					/*console.log("set map");*/
					content.avgInstituteArray=content.avgInstitute.toArray();
					for(l=content.avgInstituteArray.length-1,m=0;m<3;m++,l--)
					{
						if(content.avgInstituteArray[l]!="")
						{
							content.topThree.push(content.avgInstituteArray[l]);
							content.Score.push(content.institutions[content.avgInstituteArray[l]].average);
						}
					}
					$("#spinner").addClass('hidden');
					$("#leaderContent").removeClass('hidden');
				}
			}.bind(this),5000);

		},
		watch:{
			participatedStudents:function(value)
			{
				content.studentTableData=[];
				value.forEach(function(student){
					var arr=[student['rating'],student['username'],
						student['totalScore']];
					content.studentTableData.push(arr);
				});
				//console.log(content.studentTableData);
				createStudentTable();
			}
		},
		computed:{
			participatedStudents:function()
			{
				/*console.log("test");*/
				try{
					if(this.showParticipatedStudents==='')
					{
						/*console.log("blank");*/
						return [];
					}
					else
					{
						//console.log("participatedStudents");
						return this.output.filter(student => {
							//console.log(student['institution']+" : "+(student['institution']===this.showParticipatedStudents))
							if(student['institution']===this.showParticipatedStudents)
								return true;
							else
								return false;
						});
					}
				}catch(e)
				{
					console.log(e);
				}
			}
		},
		methods:{
			showStudents:function(institute)
			{
				$('#leaderContent').addClass('hidden');
				$('#studentContent').removeClass('hidden');
				this.showParticipatedStudents=institute;

			},
			showLeaderBoard:function()
			{
				$('#leaderContent').removeClass('hidden');
				$('#studentContent').addClass('hidden');
			},
			reload:function()
			{
				window.location.reload();
			},
		}
	});
	var dataTable=$('#studentTable').DataTable({
				"data":content.studentTableData,
	            "order": [[ 0, "desc" ]],
	            "pageLength": 100,
	            "autoWidth": false,
			});
	var timeLeft = 3;
	var elem = document.getElementById('timer');

	var timerId = setInterval(countdown, 1000);

	function countdown() {
	  if (timeLeft == 0) {
	    clearTimeout(timerId);
	    doSomething();
	  } else {
	    elem.innerHTML = timeLeft + ' seconds remaining';
	    timeLeft--;
	  }
	}
	function createStudentTable()
	{
		//console.log("createtable");
		dataTable.clear().draw();
		dataTable.rows.add(content.studentTableData); // Add new data
		dataTable.columns.adjust().draw(); // Redraw the DataTable
	}





	// 5 minutes from now
var time_in_minutes = 5;
var current_time = Date.parse(new Date());
var deadline = new Date(current_time + time_in_minutes*60*1000);


function time_remaining(endtime){
	var t = Date.parse(endtime) - Date.parse(new Date());
	var seconds = Math.floor( (t/1000) % 60 );
	var minutes = Math.floor( (t/1000/60) % 60 );
	var hours = Math.floor( (t/(1000*60*60)) % 24 );
	var days = Math.floor( t/(1000*60*60*24) );
	return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
}
function run_clock(id,endtime){
	var clock = document.getElementById(id);
	function update_clock(){
		var t = time_remaining(endtime);
		clock.innerHTML = 'Please Wait for '+t.minutes+' minutes and '+t.seconds+" seconds";
		if(t.minutes==0 && t.seconds==0)
			window.location.reload();
		if(t.total<=0){ clearInterval(timeinterval); }
	}
	update_clock(); // run function once at first to avoid delay
	var timeinterval = setInterval(update_clock,1000);
}
function doSomething()
{
	return;
}
run_clock('clockdiv',deadline);
</script>
</body>
</html>