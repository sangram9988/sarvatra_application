<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <?php
    require('ExternalLinks.php');
    ?>
</head>
<body>
<style type="text/css">
	 html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
	#background{
		width: 100%;
            height: 100%;
            max-height: 100%;
            margin: 0;
            padding: 0;
            background-size:100% 100%;
            background-repeat: no-repeat;
			background-image: url('/images/code.jpg');
	}
</style>
<div id="background">
	<div style="display: flex;justify-content: center;align-items: center;height: 100%;width:100%; background-image: linear-gradient(#00000038, black);">
          <a href="/AllContests.php" class="button is-large is-success">Login to Use the Application</a>
	</div>
</div>
</body>
</html>