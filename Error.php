<!DOCTYPE html>
<html>
<head>
	<title>Error</title>
	<?php require "ExternalLinks.php"?>
</head>
<body >
<style type="text/css">
#header{
	background-color :#2b3b90;
	font-size: 14px !important;
	color:white;
	padding:1%;
	margin-bottom: 1%;
	display:flex;
}
html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
</style>
<div id="header">
	<div style="flex-basis: 50%;">
		Codechef Contests
	</div>
	<div style="display: flex;justify-content: flex-end;flex-basis: 50%;">
		<div style="margin:0% 3%;" id="contest">
			<a href="/AllContests.php" style="color:white;">All Contests</a>
		</div>
	</div>
</div>
<div style="height: 100%;width:100%;display: flex;justify-content: center;align-items: center;flex-direction: column;">
<h1>Something went wrong !!!</h1>
<h1>Retry after <a href="/index.php">Login</a></h1>
</div>
</body>
</html>