<?php

namespace Main;
try {
	class Authentication {
		public $clientId = "b4ae785f5e380bd7655f691a3c4eb7b6";
		public $clientSecret = "a659ef91d55b42f0402a346df2457df6";
		public $apiEndpoint = 'https://api.codechef.com/';
		public $authorizationCodeEndpoint = 'https://api.codechef.com/oauth/authorize';
		public $accessTokenEndpoint = 'https://api.codechef.com/oauth/token';
		public $redirectUri = 'http://149.129.145.143/redirect.php';
		public $websiteBaseUrl = 'http://149.129.145.143/';
		public $authorizationCode = "";

		public function __construct() {
			session_start();
			if (isset($_GET['code'])) {
				$this->authorizationCode = $_GET['code'];
				$oauthDetails = $this->generate_access_token_first_time();
			} else {
				if (stripos($_SERVER['REQUEST_URI'], "redirect.php") === FALSE) {
					$_SESSION['url'] = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
				}

				$this->checkTokens();
			}

		}
		function make_curl_request($url, $post = FALSE, $headers = array()) {
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

			if ($post) {
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
			}

			$headers[] = 'content-Type: application/json';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$response = curl_exec($ch);
			curl_close($ch);
			return $response;
		}
		function generate_access_token_first_time() {

			$oauthConfig = array('grant_type' => 'authorization_code',
				'code' => $this->authorizationCode,
				'client_id' => $this->clientId,
				'client_secret' => $this->clientSecret,
				'redirect_uri' => $this->redirectUri);
			$response = json_decode($this->make_curl_request($this->accessTokenEndpoint, $oauthConfig), true);
			$result = $response['result']['data'];

			$_SESSION['accessToken'] = $result['access_token'];
			$_SESSION['refreshToken'] = $result['refresh_token'];
			return true;
		}
		public function checkTokens() {
			if (!isset($_SESSION['accessToken']) && !isset($_SESSION['refreshToken'])) {
				$this->authenticate();
			} else if (isset($_SESSION['refreshToken'])) {
				$this->generate_access_token_from_refresh_token();
			}
		}
		function generate_access_token_from_refresh_token() {

			$oauthConfig = array('grant_type' => 'refresh_token', 'refresh_token' => $_SESSION['refreshToken'], 'client_id' => $this->clientId, 'client_secret' => $this->clientSecret);
			$response = json_decode($this->make_curl_request($this->accessTokenEndpoint, $oauthConfig), true);

			$result = $response['result']['data'];
			/*echo  var_dump($_SESSION['refreshToken'])."\n";*/

			$_SESSION['accessToken'] = $result['access_token'];
			$_SESSION['refreshToken'] = $result['refresh_token'];
			/*echo var_dump($_SESSION['refreshToken']);*/
			return true;
		}
		public function authenticate() {
			$params = array('response_type' => 'code', 'client_id' => $this->clientId, 'redirect_uri' => $this->redirectUri, 'state' => 'xyz');
			header('Location: ' . $this->authorizationCodeEndpoint . '?' . http_build_query($params));
		}

	}
} catch (Exception $e) {
	echo $e;
	die();
}
?>